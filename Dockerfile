FROM node:10-alpine as builder
COPY package.json package-lock.json ./
RUN npm install && mkdir /hw-11-docker && mv ./node_modules ./hw-11-docker
WORKDIR /hw-11-docker
COPY . .
RUN npm run build

FROM nginx:alpine
#!/bin/sh
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /hw-11-docker/build /usr/share/nginx/html
EXPOSE 3000 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]