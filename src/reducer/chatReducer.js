import {
  SET_CHAT_DATA,
  UPDATE_MESSAGE_LIKES,
  EDIT_MESSAGE,
  ADD_MESSAGE,
  DELETE_MESSAGE
} from '../actions/actionTypes';

import {
  setChatData,
  updateMessageLikes,
  editMessage,
  addMessage,
  deleteMessage
} from '../helpers/reducerHelper';

const initState = {
  isLoading: true,
  chatName: '',
  user: {},
  messages: [],
}

export default function chatReducer(state = initState, { type, payload }) {
  switch (type) {
    case SET_CHAT_DATA:
      return setChatData(state, payload);
    
    case UPDATE_MESSAGE_LIKES:
      return updateMessageLikes(state, payload);
    
    case EDIT_MESSAGE:
      return editMessage(state, payload);
    
    case ADD_MESSAGE:
      return addMessage(state, payload);
    
    case DELETE_MESSAGE:
      return deleteMessage(state, payload);
    
    default:
      return state;
  }
}