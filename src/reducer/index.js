import { combineReducers } from 'redux';
import chatReducer from './chatReducer';
import editMessagePopupReducer from './editMessagePopupReducer';

const combinedReducer = combineReducers({
  chat: chatReducer,
  editMessagePopup: editMessagePopupReducer,
});

export default combinedReducer;