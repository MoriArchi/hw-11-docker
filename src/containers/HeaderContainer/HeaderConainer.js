import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Header from '../../components/Header';

import {
  messagesCountSelector,
  partipiantsCountSelector,
  lastMessageTimeSelector,
  chatNameSelector,
} from '../../selectors';

function HeaderContainer({
  chatName,
  partipiantsCount,
  messagesCount,
  lastMessageTime,
}) {

  return (
    <Header
      chatName={chatName}
      partipiantsCount={partipiantsCount}
      messagesCount={messagesCount}
      lastMessageTime={lastMessageTime}
    />
  )
}

const mapStateToProps = state => ({
    chatName: chatNameSelector(state),
    partipiantsCount: partipiantsCountSelector(state),
    messagesCount: messagesCountSelector(state),
    lastMessageTime: lastMessageTimeSelector(state),
});

HeaderContainer.propTypes = {
  chatName: PropTypes.string.isRequired,
  partipiantsCount: PropTypes.number.isRequired,
  messagesCount: PropTypes.number.isRequired,
  lastMessageTime: PropTypes.string.isRequired,
}

export default connect(mapStateToProps)(HeaderContainer);