import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Message from '../Message';
import './messageList.css';

class MessageList extends Component {

  renderMessageItems() {
    const { currentUserId, messages, onMessageLike, onShowEditMessagePopup, onMessageDelete } = this.props;

    let messagesDate;

    const messageItems = messages.map(message => {
      const { id, date, userId, ...messageProps } = message;

      const isDifferentDate = messagesDate !== date;
      const isOwnMessage = currentUserId === userId;

      if (isDifferentDate) {
        messagesDate = date;
      }

      return (
        <React.Fragment key={id}>
          {isDifferentDate && (
            <div key={date} className="messages__date-line">
              <span>{date}</span>
             </div>
          )}
          <Message
            isOwnMessage={isOwnMessage}
            {...messageProps}
            onLike={() => onMessageLike(id)}
            onDelete={() => onMessageDelete(id)}
            onShowEditPopup={() => onShowEditMessagePopup({ id, text: messageProps.text })}
          />
        </React.Fragment>
      );
    });
    return messageItems;
  }

  render() {
    const { messagesBottomRef } = this.props;

    return (
      <div className="messages">
        {this.renderMessageItems()}
        <div ref={messagesBottomRef}></div>
      </div>
    );
  }
}

MessageList.propTypes = {
  currentUserId: PropTypes.string.isRequired,
  messages: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    isLikedByCurrentUser: PropTypes.bool.isRequired,
    editedAt: PropTypes.string.isRequired,
  })),
  onMessageLike: PropTypes.func.isRequired,
  onShowEditMessagePopup: PropTypes.func.isRequired,
  onMessageDelete: PropTypes.func.isRequired,
};

export default MessageList;