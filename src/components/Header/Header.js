import React from 'react';
import PropTypes from 'prop-types';
import './header.css';

const Header = ({ chatName, partipiantsCount, messagesCount, lastMessageTime }) => {
  return (
    <header className="chat-header">
      <div className="chat-info">
        <div className="header__chat-name">{chatName}</div>
        <div className="header__partipians">
          <p><i className="fa fa-user icon" />
            {partipiantsCount} partipiants
            </p>
        </div>
        <div className="header__messages">
          <p><i className="fa fa-comments icon" />
            {messagesCount} messages
            </p>
        </div>
      </div >
      <div className="header__last-message">
        <p><i className="fa fa-clock-o icon" />
          Last message at  {lastMessageTime}
        </p>
      </div>
    </header >
  );
};

Header.propTypes = {
  headerInfo: PropTypes.shape({
    chatName: PropTypes.string.isRequired,
    partipiantsCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.number.isRequired
  })
};

export default Header;
