import moment from 'moment';
import TIME_FORMAT from '../constants/timeFormat';

export const getPartipiantsCount = messages => messages.length
  ? new Set(messages.map(message => message.user)).size
  : 0;

export const getLastMessageTime = messages => messages.length
  ? moment(messages[messages.length - 1].createdAt, TIME_FORMAT).format('Do MMMM HH:mm')
  : null;

export const getUserLastMessage = (messages, userId) => {
  const allUserMessages = messages.filter(message => message.userId === userId);
  
  if (allUserMessages.length) {
    const { id, text } = allUserMessages[allUserMessages.length - 1];

    return { isMessageExist: true, id, text };
  }

  return { isMessageExist: false, id: '', text: '' };
};
