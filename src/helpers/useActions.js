import { bindActionCreators } from 'redux';
import { useDispatch } from 'react-redux';
import { useMemo } from 'react';

export default function useActions(...actions) {
  const dispatch = useDispatch();
  
  return useMemo(
    () => actions.map(a => bindActionCreators(a, dispatch)),
    [dispatch] // eslint-disable-line react-hooks/exhaustive-deps
  );
}